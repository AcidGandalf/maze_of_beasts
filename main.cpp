#include "engine/easy.h"
#include <fstream>
#include <iomanip>

using namespace arctic;  // NOLINT
using namespace ::std;

// Function prototypes.
bool InitGraphics();
void OnKeyDown(Ui32 wParam);
void OnDestroy();
void OnLButtonDown(Ui32 x, Ui32 y);
void OnLButtonUp(Ui32 x, Ui32 y);
void OnPauseTimer();
void OnTimer();
void OnChar(Ui32 symbol);
void PaintScreen();
void DisplayNumber(char* s, Ui32 x, Ui32 y);
void StartANewGame();
void RestartCurrentLevel();
void LoadLevel();
void GameOver();
void GameComplete();
void MoveLeft();
void MoveRight();
void MoveUp();
void MoveDown();
void MakeMove();
void LevelComplete();
void TvarusMakesMove();
void Tvarus2MakesMove();
void Tvarus3MakesMove();
void DisplayAllNum();
// he makes it step-by step.
int FindDistance2(int checkingX,int checkingY);
void MakeMoveToMaxDist();

void Tvarus4MakesMove();
void SetTvarusDirection(int XMT,int YMT);
void SetTvarus2Direction(int XMT,int YMT);
void SetTvarus3Direction(int XMT,int YMT);
void SetTvarus4Direction(int XMT,int YMT);
// TableOfResults
void LoadTableOfResults();
void ShowTableOfResults();
void InputTableOfResults();
void SaveTableOfResults();
void ShowName(char NameToShow[], int X, int Y);
void DisplayNumbers2(char* s, Ui32 x, Ui32 y);
void InputHiSteps(int stringNum);
void OnSymbolEnteredInTable(Ui32 wParam);

//Sound...
bool InitSound();
Ui32 LoadSound(char* filename);
void SayStep();
void SayPush();
void SayShip();
void SayDeath();
void SayThunder();

// Global variables.
bool ddok;
bool setupOK = false;

// Surface Numbers
Sprite partsASurfaceNum;
Sprite levelSurfaceNum;
Sprite lifesSurfaceNum;
Sprite stepsSurfaceNum;
Sprite partsESurfaceNum;
Sprite numbers1SurfaceNum;
Sprite numbers2SurfaceNum;
Sprite restartSurfaceNum;
Sprite newSurfaceNum;
Sprite wallSurfaceNum;
Sprite wall2SurfaceNum;
Sprite wall3SurfaceNum;
Sprite wall4SurfaceNum;
Sprite wall5SurfaceNum;
Sprite cloudSurfaceNum;
Sprite crossSurfaceNum;
Sprite boxSurfaceNum;
Sprite manSurfaceNum;
Sprite cboxSurfaceNum;
Sprite shipSurfaceNum;
Sprite holeSurfaceNum;
Sprite evilSurfaceNum;
Sprite bevilSurfaceNum;
Sprite hevilSurfaceNum;
Sprite mevilSurfaceNum;
Sprite letters1SurfaceNum;
Sprite pauseSurfaceNum;
Sprite green1SurfaceNum;
Sprite backgrndSurfaceNum;
Sprite cloud1SurfaceNum;
Sprite cloud2SurfaceNum;
Sprite cloud3SurfaceNum;
Sprite lightSurfaceNum;
Sprite stone1SurfaceNum;
Sprite red1SurfaceNum;


// Button Indicators
bool exitOK = false;
bool restartOK = false;
bool newOK = false;
// Current level number
Ui32 levelNum=1;
// Lifes left
Ui32 lifesNum=4;
// Steps Done
Ui32 stepsNum=0;
// Flag Of LevelComplete
bool levelComplete = false;
// Flag Of GameOver
bool gameOver = false;
// Flag Of Secret
bool secret = false;
// Main Flag of SECRET
bool mainSecret = false;
// Show Xes
bool showXES = false;
// Quad Of Level Massive
const char CLEAR = '.', WALL = '*', WALL2 = '%', WALL3 = '$',
WALL4 = 't', WALL5 = 'v', SHIP = 'I', HOLE = 'i', HMAN = 'W',
CROSS = '+', MAN = 'A', CBOX = '@', CMAN = 'S', BOX = '#',
XES = 'X', TVARUS = 'T', TVARUS2 = '2', TVARUS3 = '3',
TVARUS4 ='4', CLOUD = 'C', FUTURES = 'f';

char currentLevel[35][25];
char futuresMap[35][25];
char turesMap[35][25];
// Man's Coordinates
int MansX;
int MansY;
// Tvarus Coordinates
int TvarusX;
int TvarusY;
// Tvarus2 Coordinates
int Tvarus2X;
int Tvarus2Y;
// Tvarus3 Coordinates
int Tvarus3X;
int Tvarus3Y;

// Tvarus3 HELPERS
int MaxDiffer;
int MaxDifferX;
int MaxDifferY;

// Tvarus4 Coordinates
int Tvarus4X;
int Tvarus4Y;
// Mans Direction (and tvarus)
const Ui32 UP = 2, DOWN = 0, LEFT = 1, RIGHT = 3;

Ui32 MansD=UP;
Ui32 TvarusD=RIGHT;
Ui32 Tvarus2D=RIGHT;
Ui32 Tvarus3D=RIGHT;
Ui32 Tvarus4D=RIGHT;
// Mans Hands Up
bool MansHU = false;
// Tvarus Eye Move
bool TvarusEM = false;
// Man's Move
int MX=0;
int MY=0;
// FREE BOXES ?
bool FreeBoxes;

// Tvarus On LEVEL ?
bool tvarusActive = false;
// Tvarus2 On LEVEL ?
bool tvarus2Active = false;
// Tvarus3 On LEVEL ?
bool tvarus3Active = false;
// Tvarus4 On LEVEL ?
bool tvarus4Active = false;

// TableOfResults
char hiName1[15];
char hiName2[15];
char hiName3[15];
Ui32 hiSteps1;
Ui32 hiSteps2;
Ui32 hiSteps3;

// We'll create a pointer to point the array,
// we need to enter.
char *entName;

// Flag Of entering result in the table.
bool enteringResult = false;

// Flag, that shows, shold PaintScreen() launch table-drawing.
bool menuOnline = true;

// Flag, that shows, shold menuOnline become false after any key.
bool menuOff = true;

// Number of currently entering letter.
int enteringLetter;

//Sound...
Sound helloBufferNum;
Sound goodbyeBufferNum;
Sound steps1BufferNum;
Sound steps2BufferNum;
Sound steps3BufferNum;
Sound push1BufferNum;
Sound push2BufferNum;
Sound push3BufferNum;
Sound ship1BufferNum;
Sound ship2BufferNum;
Sound nameBufferNum;
Sound death1BufferNum;
Sound death2BufferNum;
Sound death3BufferNum;
Sound healBufferNum;
Sound thunderBufferNum;
Sound thunder1BufferNum;

// Pause.
bool pauseOn = false;


double kUpdatePeriod = 0.4;
double kPauseUpdatePeriod = 0.1;

bool doExit = false;

void LimitRate(double frame_start) {
  while (true) {
    float dur = 1.f/121.f - (float)(Time() - frame_start);
    if (dur > 0.f) {
      Sleep(dur);
    } else {
      break;
    }
  }
}

void BlitImage(Vec2Si32 pos, Sprite &sprite, Vec4Si32 rect) {
  Si32 left = rect.x;
  Si32 top = rect.y;
  Si32 right = rect.z;
  Si32 bottom = rect.w;
  Si32 wid = right - left;
  Si32 hei = bottom - top;
  
  Si32 sh = ScreenSize().y - 1;
  sprite.Draw(pos.x, sh - pos.y - hei, wid, hei, left, top, wid, hei);
}

void ShowTableOfResultsLoop() {
  double t1 = Time();
  double t2 = Time();
  double timerAcc = 0.0;
  OnPauseTimer();
  ShowTableOfResults();
  ShowFrame();
  while (!IsAnyKeyDownward()) {
    t1 = t2;
    t2 = Time();
    double dt = t2 - t1;
    
    timerAcc += dt;
    if (timerAcc > kPauseUpdatePeriod) {
      timerAcc = 0.0;
      OnPauseTimer();
      ShowTableOfResults();
    }
    LimitRate(t1);
    ShowFrame();
  }
}

void PauseScreenLoop() {
  double t1 = Time();
  double t2 = Time();
  double timerAcc = 0.0;
  OnPauseTimer();
  ShowFrame();
  while (!IsAnyKeyDownward()) {
    t1 = t2;
    t2 = Time();
    double dt = t2 - t1;

    timerAcc += dt;
    if (timerAcc > kPauseUpdatePeriod) {
      timerAcc = 0.0;
      OnPauseTimer();
    }
    LimitRate(t1);
    ShowFrame();
  }
}


void EasyMain() {
  ResizeScreen(640, 480);
  
  setupOK = InitGraphics();
  if (!setupOK)
    return;
  setupOK = InitSound();
  if (!setupOK)
    return;
  
  StartANewGame();

  double t1 = Time();
  double t2 = Time();
  double timerAcc = 0.0;
  while (!doExit) {
    t1 = t2;
    t2 = Time();
    double dt = t2 - t1;
    
    if (pauseOn) {
      timerAcc += dt;
      if (timerAcc > kPauseUpdatePeriod) {
        timerAcc = 0.0;
        OnPauseTimer();
      }
    } else {
      timerAcc += dt;
      if (timerAcc > kUpdatePeriod) {
        timerAcc = 0.0;
        OnTimer();
      }
    }
    
    for (Si32 i = 0; i < InputMessageCount(); ++i) {
      const InputMessage& msg = GetInputMessage(i);
      if (msg.kind == InputMessage::kKeyboard) {
        if (msg.keyboard.key_state == 1) {
          OnKeyDown(msg.keyboard.key);
        } else {
          OnChar(msg.keyboard.key);
        }
      } else {
        if (msg.keyboard.key == kKeyMouseLeft) {
          if (msg.keyboard.key_state == 1) {
            OnLButtonDown(MousePos().x, ScreenSize().y - 1 - MousePos().y);
          } else {
            OnLButtonUp(MousePos().x, ScreenSize().y - 1 - MousePos().y);
          }
        }
      }
    }
    
    LimitRate(t1);
    ShowFrame();
  }
}

void StartANewGame() {
  stepsNum = 0;
  lifesNum = 4;
  levelNum = 1;
  secret = false;
  gameOver = false;
  mainSecret = false;
  //LoadResults;
  LoadTableOfResults();
  
  ShowTableOfResultsLoop();
  
  
  menuOnline = true;
  menuOff = true;
  
  RestartCurrentLevel();
}

void RestartCurrentLevel() {
  if (lifesNum == 0) {
    GameOver();
    return;
  }
  lifesNum--;
  LoadLevel();
  PaintScreen();
}

void LoadLevel() {
  for (int ymas=0; ymas < 25; ymas++)
    for (int xmas=0; xmas < 35; xmas++) {
      futuresMap[xmas][ymas] = CLEAR;
    }
  int ymas;
  for (ymas=0; ymas < 25; ymas++)
    for (int xmas=0; xmas < 35; xmas++) {
      turesMap[xmas][ymas] = CLEAR;
    }
  
  MansHU = false;
  if (levelNum >= 1 && levelNum <= 7) {
    char filename[100];
    sprintf(filename, "data/level%d.dat", (int)levelNum);
    char number;
    ifstream fin(filename);
    for (int ymas=0; ymas < 25; ymas++)
      for (int xmas=0; xmas < 35; xmas++) {
        fin >> number;
        currentLevel[xmas][ymas] = number;
      }
    fin.close();
    
    sprintf(filename, "data/futures%d.dat", (int)levelNum);
    ifstream ffin(filename);
    for (ymas=0; ymas < 25; ymas++)
      for (int xmas=0; xmas < 35; xmas++) {
        ffin >> number;
        futuresMap[xmas][ymas] = number;
      }
    ffin.close();
    
    sprintf(filename, "data/tures%d.dat", (int)levelNum);
    ifstream f0fin(filename);
    for (ymas=0; ymas < 25; ymas++)
      for (int xmas=0; xmas < 35; xmas++) {
        f0fin >> number;
        turesMap[xmas][ymas] = number;
      }
    f0fin.close();
  }
  
  
  if (levelNum == 8)
    GameComplete();
  
  // SECRET !!!
  if (secret) {
    char number;
    ifstream fin("data/level0.dat");
    for (int ymas = 0; ymas < 25; ymas++)
      for (int xmas = 0; xmas < 35; xmas++) {
        fin >> number;
        currentLevel[xmas][ymas] = number;
      }
    fin.close();
    
    ifstream ffin("data/futures0.dat");
    for (ymas = 0; ymas < 25; ymas++)
      for (int xmas = 0; xmas < 35; xmas++) {
        ffin >> number;
        futuresMap[xmas][ymas] = number;
      }
    ffin.close();
    
    ifstream f0fin("data/tures0.dat");
    for (ymas = 0; ymas < 25; ymas++)
      for (int xmas = 0; xmas < 35; xmas++) {
        f0fin >> number;
        turesMap[xmas][ymas] = number;
      }
    f0fin.close();
    
    gameOver = false;
    mainSecret = true;
    PaintScreen();
    return;
  }
  levelComplete = false;
  PaintScreen();
  return;
}

void GameOver() {
  gameOver = true;
  char number;
  ifstream fin("data/gameover.dat");
  for (int ymas = 0; ymas < 25; ymas++)
    for (int xmas = 0; xmas < 35; xmas++) {
      fin >> number;
      currentLevel[xmas][ymas] = number;
    }
  fin.close();
  showXES = true;
  PaintScreen();
  ShowFrame();
  Sleep(0.016);
  showXES = false;
  PaintScreen();
  return;
}

void LevelComplete() {
  if (mainSecret)
    levelNum--;
  mainSecret = false;
  secret = false;
  
  // GAME OVER COMPLETE !!!!
  if (gameOver) {
    char number;
    ifstream fin("data/secret.dat");
    for (int ymas = 0; ymas < 25; ymas++)
      for (int xmas = 0; xmas < 35; xmas++) {
        fin >> number;
        currentLevel[xmas][ymas] = number;
      }
    fin.close();
    secret = true;
    levelComplete=true;
    lifesNum = 2;
    gameOver = false;
    PaintScreen();
    return;
  }
  
  levelNum++;
  levelComplete = true;
  char number;
  ifstream fin("data/complete.dat");
  for (int ymas = 0; ymas < 25; ymas++)
    for (int xmas = 0; xmas < 35; xmas++) {
      fin >> number;
      currentLevel[xmas][ymas] = number;
    }
  fin.close();
  PaintScreen();
  return;
}

void OnLButtonDown(Ui32 x, Ui32 y) {
  if (menuOff) {
    menuOff = false;
    menuOnline = false;
    PaintScreen();
    return;
  }
  if (enteringResult)
    return;
  if (levelComplete) {
    levelComplete = false;
    LoadLevel();
    return;
  }

  // EXIT Button pressed
  if (x > 545 && x < 593 && y > 447 && y < 467) {
    exitOK = true;
    restartOK = false;
    newOK = false;
    PaintScreen();
    return;
  }
  
  // RESTART CURRENT GAME Button pressed
  if ((x > 261) && (x < 484) && (y > 447) && (y < 467)) {
    restartOK = true;
    exitOK = false;
    newOK = false;
    PaintScreen();
    return;
  }
  
  // START A NEW GAME Button pressed
  if ((x > 38) && (x < 209) && (y > 447) && (y < 467)) {
    newOK = true;
    restartOK = false;
    exitOK = false;
    PaintScreen();
    return;
  }
  
  // Pressed NON-GAME Space
  exitOK = false;
  restartOK = false;
  newOK = false;
  PaintScreen();
}

void OnLButtonUp(Ui32 x, Ui32 y) {
  if (menuOff) {
    menuOff = false;
    menuOnline = false;
    PaintScreen();
    return;
  }
  if (enteringResult)
    return;

  // EXIT Button unpressed
  if (exitOK && (x > 545) && (x < 593) && (y > 447) && (y < 467)) {
    // Say godbye.
    goodbyeBufferNum.Play();
    Sleep(1.6);
    doExit = true;
    return;
  }
  
  // RESTART CURRENT GAME Button unpressed
  if (restartOK && (x > 261) && (x < 484) && (y > 447) && (y < 467)) {
    RestartCurrentLevel();
  }
  
  // START A NEW GAME Button unpressed
  if (newOK && (x > 38) && (x < 209) && (y > 447) && (y < 467)) {
    StartANewGame();
  }
  
  exitOK = false;
  restartOK = false;
  newOK = false;
  
  PaintScreen();
}

void OnKeyDown(Ui32 wParam) {
  if (enteringResult == true) {
    OnSymbolEnteredInTable(wParam);
    return;
  }
  
  if (menuOff==true) {
    menuOff=false;
    menuOnline=false;
    PaintScreen();
    return;
  }
  
  if (levelComplete) {
    levelComplete = false;
    LoadLevel();
    return;
  }
  
  if (pauseOn == true) {
    pauseOn = false;
    PaintScreen();
    return;
  }
  
  if (wParam == kKeyEscape) {
    if (!exitOK && !restartOK && !newOK) {
      exitOK = true;
      restartOK = false;
      newOK = false;
      PaintScreen();
      return;
    }
    exitOK = false;
    restartOK = false;
    newOK = false;
    PaintScreen();
    return;
  }
  
  if (wParam == kKeyLeft) {
    if (exitOK) {
      exitOK = false;
      restartOK = true;
      PaintScreen();
      return;
    }
    
    if (restartOK) {
      restartOK = false;
      newOK = true;
      PaintScreen();
      return;
    }
    
    if (newOK) {
      newOK = false;
      exitOK = true;
      PaintScreen();
      return;
    }
    
    MoveLeft();
  }
  
  if (wParam == kKeyRight) {
    if (newOK) {
      newOK = false;
      restartOK = true;
      PaintScreen();
      return;
    }
    
    if (restartOK) {
      restartOK = false;
      exitOK = true;
      PaintScreen();
      return;
    }
    
    if (exitOK) {
      exitOK = false;
      newOK = true;
      PaintScreen();
      return;
    }
    MoveRight();
    return;
  }
  
  if (wParam == kKeyUp) {
    MoveUp();
    return;
  }
  
  if (wParam == kKeyDown) {
    MoveDown();
    return;
  }
  
  if (wParam == 'P') {
    pauseOn = true;
    PaintScreen();
    return;
  }
  
  if (wParam == kKeyEnter) {
    if (!exitOK && !restartOK && !newOK)
      return;
    
    if (exitOK) {
      exitOK = false;
      // Say goodbye.
      goodbyeBufferNum.Play();
      Sleep(1.6);
      doExit = true;
      return;
    }
    
    if (restartOK) {
      restartOK = false;
      RestartCurrentLevel();
    }
    
    if (newOK) {
      newOK = false;
      StartANewGame();
    }
    PaintScreen();
  }
}

void MoveLeft() {
  MX = -1;
  MY = 0;
  MansD = LEFT;
  MakeMove();
}

void MoveRight() {
  MX = 1;
  MY = 0;
  MansD = RIGHT;
  MakeMove();
}

void MoveUp() {
  MY = -1;
  MX = 0;
  MansD = UP;
  MakeMove();
}

void MoveDown() {
  MY = 1;
  MX = 0;
  MansD = DOWN;
  MakeMove();
}

void MakeMove() {
  MansHU = false;
  // CLEAR is on Your WAY
  if (currentLevel[MansX + MX][MansY + MY] == CLEAR) {
    // You Are a MAN
    if (currentLevel[MansX][MansY] == MAN) {
      currentLevel[MansX][MansY] = CLEAR;
      currentLevel[MansX + MX][MansY + MY] = MAN;
      SayStep();
      stepsNum++;
      PaintScreen();
      return;
    }
    // You Are a CMAN
    if (currentLevel[MansX][MansY] == CMAN) {
      currentLevel[MansX][MansY] = CROSS;
      currentLevel[MansX + MX][MansY + MY] = MAN;
      SayStep();
      stepsNum++;
      PaintScreen();
      return;
    }
    // You Are a HMAN
    if (currentLevel[MansX][MansY] == HMAN) {
      currentLevel[MansX][MansY] = SHIP;
      currentLevel[MansX + MX][MansY + MY] = MAN;
      stepsNum++;
      SayShip();
      PaintScreen();
      return;
    }
    return;
  }
  
  // CROSS is on Your WAY
  if (currentLevel[MansX + MX][MansY + MY] == CROSS) {
    // You Are a MAN
    if (currentLevel[MansX][MansY] == MAN) {
      currentLevel[MansX][MansY] = CLEAR;
      currentLevel[MansX + MX][MansY + MY] = CMAN;
      SayStep();
      stepsNum++;
      PaintScreen();
      return;
    }
    // You Are a CMAN
    if (currentLevel[MansX][MansY] == CMAN) {
      currentLevel[MansX][MansY] = CROSS;
      currentLevel[MansX + MX][MansY + MY] = CMAN;
      SayStep();
      stepsNum++;
      PaintScreen();
      return;
    }
    // You Are a HMAN
    if (currentLevel[MansX][MansY] == HMAN) {
      currentLevel[MansX][MansY] = SHIP;
      currentLevel[MansX + MX][MansY + MY] = CMAN;
      stepsNum++;
      SayShip();
      PaintScreen();
      return;
    }
    return;
  }
  
  // Tvarus3 is on Your Way
  if (currentLevel[MansX + MX][MansY + MY] == TVARUS3) {
    // You Are a MAN
    if (currentLevel[MansX][MansY] == MAN) {
      currentLevel[MansX][MansY] = CLEAR;
      currentLevel[MansX + MX][MansY + MY] = MAN;
      stepsNum++;
      tvarus3Active = false;
      lifesNum++;
      healBufferNum.Play();
      PaintScreen();
      return;
    }
    // You Are a CMAN
    if (currentLevel[MansX][MansY] == CMAN) {
      currentLevel[MansX][MansY] = CROSS;
      currentLevel[MansX + MX][MansY + MY] = MAN;
      stepsNum++;
      tvarus3Active = false;
      lifesNum++;
      healBufferNum.Play();
      PaintScreen();
      return;
    }
    // You Are a HMAN
    if (currentLevel[MansX][MansY] == HMAN) {
      currentLevel[MansX][MansY] = SHIP;
      currentLevel[MansX + MX][MansY + MY] = MAN;
      stepsNum++;
      tvarus3Active = false;
      lifesNum++;
      SayShip();
      healBufferNum.Play();
      PaintScreen();
      return;
    }
    return;
  }
  
  // HOLE is on Your WAY
  if (currentLevel[MansX + MX][MansY + MY] == HOLE) {
    // You Are a MAN
    if (currentLevel[MansX][MansY] == MAN) {
      currentLevel[MansX][MansY] = CLEAR;
      currentLevel[MansX + MX][MansY + MY] = HMAN;
      SayStep();
      stepsNum++;
      PaintScreen();
      return;
    }
    // You Are a CMAN
    if (currentLevel[MansX][MansY] == CMAN) {
      currentLevel[MansX][MansY] = CROSS;
      currentLevel[MansX + MX][MansY + MY] = HMAN;
      SayStep();
      stepsNum++;
      PaintScreen();
      return;
    }
    // You Are a HMAN
    if (currentLevel[MansX][MansY] == HMAN) {
      currentLevel[MansX][MansY] = SHIP;
      currentLevel[MansX + MX][MansY + MY] = HMAN;
      stepsNum++;
      SayShip();
      PaintScreen();
      return;
    }
    
    return;
  }
  
  // Box Is On Your Way
  if (currentLevel[MansX + MX][MansY + MY] == BOX) {
    // CLEAR Behind a Box
    if (currentLevel[MansX + 2 * MX][MansY + 2 * MY] == CLEAR) {
      // You Are A MAN
      if (currentLevel[MansX][MansY] == MAN) {
        currentLevel[MansX][MansY] = CLEAR;
        currentLevel[MansX + MX][MansY + MY] = MAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = BOX;
        stepsNum++;
        MansHU = true;
        SayPush();
        PaintScreen();
        return;
      }
      // You Are a CMAN
      if (currentLevel[MansX][MansY] == CMAN) {
        currentLevel[MansX][MansY] = CROSS;
        currentLevel[MansX + MX][MansY + MY] = MAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = BOX;
        stepsNum++;
        MansHU = true;
        SayPush();
        PaintScreen();
        return;
      }
      // You Are a HMAN
      if (currentLevel[MansX][MansY] == HMAN) {
        currentLevel[MansX][MansY] = SHIP;
        currentLevel[MansX + MX][MansY + MY] = MAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = BOX;
        stepsNum++;
        MansHU = true;
        SayShip();
        PaintScreen();
        return;
      }
      return;
    }
    // CROSS Behind a Box
    if (currentLevel[MansX + 2 * MX][MansY + 2 * MY] == CROSS) {
      // You are a MAN
      if (currentLevel[MansX][MansY] == MAN) {
        currentLevel[MansX][MansY] = CLEAR;
        currentLevel[MansX + MX][MansY + MY] = MAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = CBOX;
        stepsNum++;
        MansHU = true;
        SayPush();
        PaintScreen();
        return;
      }
      //You Are a CMAN
      if (currentLevel[MansX][MansY] == CMAN) {
        currentLevel[MansX][MansY] = CROSS;
        currentLevel[MansX + MX][MansY + MY] = MAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = CBOX;
        stepsNum++;
        MansHU = true;
        SayPush();
        PaintScreen();
        return;
      }
      //You Are a HMAN
      if (currentLevel[MansX][MansY] == HMAN) {
        currentLevel[MansX][MansY] = SHIP;
        currentLevel[MansX + MX][MansY + MY] = MAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = CBOX;
        stepsNum++;
        MansHU = true;
        SayShip();
        PaintScreen();
        return;
      }
      return;
    }
  }
  
  // CBox Is On Your Way
  if (currentLevel[MansX + MX][MansY + MY] == CBOX) {
    // CLEAR Behind a CBOX
    if (currentLevel[MansX + 2 * MX][MansY + 2 * MY] == CLEAR) {
      if (currentLevel[MansX][MansY] == MAN) {
        currentLevel[MansX][MansY] = CLEAR;
        currentLevel[MansX + MX][MansY + MY] = CMAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = BOX;
        stepsNum++;
        MansHU = true;
        SayPush();
        PaintScreen();
        return;
      }
      // U R a CMAN
      if (currentLevel[MansX][MansY] == CMAN) {
        currentLevel[MansX][MansY] = CROSS;
        currentLevel[MansX + MX][MansY + MY] = CMAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = BOX;
        stepsNum++;
        MansHU = true;
        SayPush();
        PaintScreen();
        return;
      }
      // U R a HMAN
      if (currentLevel[MansX][MansY] == HMAN) {
        currentLevel[MansX][MansY] = SHIP;
        currentLevel[MansX + MX][MansY + MY] = CMAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = BOX;
        stepsNum++;
        MansHU = true;
        SayShip();
        PaintScreen();
        return;
      }
      return;
    }
    // CROSS Behind a CBox
    if (currentLevel[MansX + 2 * MX][MansY + 2 * MY] == CROSS) {
      if (currentLevel[MansX][MansY] == MAN) {
        currentLevel[MansX][MansY] = CLEAR;
        currentLevel[MansX + MX][MansY + MY] = CMAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = CBOX;
        stepsNum++;
        MansHU = true;
        SayPush();
        PaintScreen();
        return;
      }
      // U R a CMAN
      if (currentLevel[MansX][MansY] == CMAN) {
        currentLevel[MansX][MansY] = CROSS;
        currentLevel[MansX + MX][MansY + MY] = CMAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = CBOX;
        stepsNum++;
        MansHU = true;
        SayPush();
        PaintScreen();
        return;
      }
      // U R a HMAN
      if (currentLevel[MansX][MansY] == HMAN) {
        currentLevel[MansX][MansY] = SHIP;
        currentLevel[MansX + MX][MansY + MY] = CMAN;
        currentLevel[MansX + 2 * MX][MansY + 2 * MY] = CBOX;
        stepsNum++;
        MansHU = true;
        SayShip();
        PaintScreen();
        return;
      }
      return;
    }
  }
  PaintScreen();
  return;
}

bool InitGraphics() {
  partsASurfaceNum.Create(562, 28);
  partsESurfaceNum.Create(55, 28);

  partsASurfaceNum.Load("data/gmenu.tga");
  partsESurfaceNum.Load("data/exitok.tga");
  stepsSurfaceNum.Load("data/steps.tga");
  levelSurfaceNum.Load("data/level.tga");
  lifesSurfaceNum.Load("data/lifes.tga");
  numbers1SurfaceNum.Load("data/numbers1.tga");
  numbers2SurfaceNum.Load("data/numbers2.tga");
  restartSurfaceNum.Load("data/restarto.tga");
  newSurfaceNum.Load("data/newok.tga");
  wallSurfaceNum.Load("data/wall.tga");
  wall2SurfaceNum.Load("data/wall2.tga");
  wall3SurfaceNum.Load("data/wall3.tga");
  wall4SurfaceNum.Load("data/wall4.tga");
  wall5SurfaceNum.Load("data/wall5.tga");
  cloudSurfaceNum.Load("data/cloud.tga");
  crossSurfaceNum.Load("data/cross.tga");
  boxSurfaceNum.Load("data/box.tga");
  cboxSurfaceNum.Load("data/cbox.tga");
  manSurfaceNum.Load("data/man.tga");
  evilSurfaceNum.Load("data/evil.tga");
  bevilSurfaceNum.Load("data/bevil.tga");
  hevilSurfaceNum.Load("data/hevil.tga");
  mevilSurfaceNum.Load("data/mevil.tga");
  shipSurfaceNum.Load("data/ship.tga");
  holeSurfaceNum.Load("data/hole.tga");
  letters1SurfaceNum.Load("data/letters1.tga");
  green1SurfaceNum.Load("data/green1.tga");
  pauseSurfaceNum.Load("data/pause.tga");
  backgrndSurfaceNum.Load("data/backgrnd.tga");
  cloud1SurfaceNum.Load("data/cloud1.tga");
  cloud2SurfaceNum.Load("data/cloud2.tga");
  cloud3SurfaceNum.Load("data/cloud3.tga");
  lightSurfaceNum.Load("data/light.tga");
  stone1SurfaceNum.Load("data/stone1.tga");
  red1SurfaceNum.Load("data/red1.tga");

  PaintScreen();
  return true;
}

void PaintScreen() {
  if (pauseOn==true)
    return;
  
  //shold PaintScreen() launch table-drawing ?
  if (menuOnline) {
    ShowTableOfResults();
    return;
  }
  
  Clear();
  // Blit background buttons to the back buffer.
  BlitImage(Vec2Si32(35, 444), partsASurfaceNum, Vec4Si32(0, 0, 562, 28));
  // Blit Level
  BlitImage(Vec2Si32(36, 13), levelSurfaceNum, Vec4Si32(0, 0, 50, 10));
  // Blit Lifes
  BlitImage(Vec2Si32(540, 13), lifesSurfaceNum, Vec4Si32(0, 0, 43, 10));
  // Blit Steps
  BlitImage(Vec2Si32(220, 13), stepsSurfaceNum, Vec4Si32(0, 0, 28, 10));
  
  // Display stepsNum
  DisplayAllNum();
  
  // Blit red EXIT Button
  if (exitOK==true) {
    BlitImage(Vec2Si32(542,444), partsESurfaceNum, Vec4Si32(0, 0, 55, 28));
  }
  
  // Blit yellow RESTART CURRENT GAME Button
  if (restartOK==true) {
    BlitImage(Vec2Si32(258,444), restartSurfaceNum, Vec4Si32(0, 0, 230, 28));
  }
  // Blit orange START A NEW GAME Button
  if (newOK==true) {
    BlitImage(Vec2Si32(35, 444), newSurfaceNum, Vec4Si32(0, 0, 178, 28));
  }
  
  // MAKE FreeBoxes false
  FreeBoxes = false;
  // Blit currentLevel[][]
  
  // CMan handler
  bool CManHandler = false;
  // HMan handler
  bool HManHandler = false;
  
  for (int xmas=0; xmas < 35; xmas++)
    for (int ymas=0; ymas < 25; ymas++) {
      CManHandler = false;
      HManHandler = false;
      if (currentLevel[xmas][ymas] == WALL || (currentLevel[xmas][ymas] == XES && showXES))
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), wallSurfaceNum, Vec4Si32(0,0,20,20));
      
      if (currentLevel[xmas][ymas] == WALL2)
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), wall2SurfaceNum, Vec4Si32(0,0,20,20));
      
      if (currentLevel[xmas][ymas] == WALL3)
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), wall3SurfaceNum, Vec4Si32(0,0,20,20));
      
      if (currentLevel[xmas][ymas] == WALL4)
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), wall4SurfaceNum, Vec4Si32(0,0,20,20));
      
      if (currentLevel[xmas][ymas] == WALL5)
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), wall5SurfaceNum, Vec4Si32(0,0,20,20));
      
      if (currentLevel[xmas][ymas] == SHIP)
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), shipSurfaceNum, Vec4Si32(0,0,20,20));
      
      if (currentLevel[xmas][ymas] == HOLE)
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), holeSurfaceNum, Vec4Si32(0,0,20,20));
      
      if (currentLevel[xmas][ymas] == CLOUD){
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), cloudSurfaceNum, Vec4Si32(0,0,20,20));
        //Make it HOLE
        currentLevel[xmas][ymas] = HOLE;
      }
      
      if (currentLevel[xmas][ymas] == CROSS)
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), crossSurfaceNum, Vec4Si32(0, 0, 16, 16));
      
      if (currentLevel[xmas][ymas] == BOX){
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), boxSurfaceNum, Vec4Si32(0, 0, 20, 20));
        FreeBoxes = true;
      }
      
      if (currentLevel[xmas][ymas] == CBOX)
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), cboxSurfaceNum, Vec4Si32(0, 0, 20, 20));
      
      if (currentLevel[xmas][ymas] == CMAN) {
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), crossSurfaceNum, Vec4Si32(0, 0, 16, 16));
        CManHandler = true;
      }
      
      if (currentLevel[xmas][ymas] == HMAN) {
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), holeSurfaceNum, Vec4Si32(0, 0, 20, 20));
        HManHandler = true;
      }
      
      if ((currentLevel[xmas][ymas] == MAN) || CManHandler || HManHandler) {
        // Set Mans Coordinates
        MansX = xmas;
        MansY = ymas;
        // Paint Man in His Position
        int DrawingAdjustment = 0;
        DrawingAdjustment = MansD;
        if (MansHU)
          DrawingAdjustment=DrawingAdjustment+4;
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), manSurfaceNum,
                  Vec4Si32(DrawingAdjustment*20, 0, DrawingAdjustment*20+20, 20));
      }
      
      if (currentLevel[xmas][ymas] == TVARUS){
        // activate Tvarus
        tvarusActive = true;
        // Set Tvarus Coordinates
        TvarusX = xmas;
        TvarusY = ymas;
        // Paint Tvarus in His Position
        int DrawingAdjustment = 0;
        DrawingAdjustment = TvarusD;
        if (TvarusEM)
          DrawingAdjustment=DrawingAdjustment+4;
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), evilSurfaceNum,
                  Vec4Si32(DrawingAdjustment*20, 0, DrawingAdjustment*20+20, 20));
      }
      if (currentLevel[xmas][ymas] == TVARUS2) {
        // activate Tvarus2
        tvarus2Active = true;
        // Set Tvarus2 Coordinates
        Tvarus2X = xmas;
        Tvarus2Y = ymas;
        // Paint Tvarus2 in His Position
        int DrawingAdjustment = 0;
        DrawingAdjustment = Tvarus2D;
        if (TvarusEM)
          DrawingAdjustment = DrawingAdjustment + 4;
        BlitImage(Vec2Si32(36 + xmas * 16, 36 + ymas * 16), bevilSurfaceNum,
                  Vec4Si32(DrawingAdjustment * 20, 0, DrawingAdjustment * 20 + 20, 20));
      }
      if (currentLevel[xmas][ymas] == TVARUS3) {
        // activate Tvarus3
        tvarus3Active = true;
        // Set Tvarus3 Coordinates
        Tvarus3X = xmas;
        Tvarus3Y = ymas;
        // Paint Tvarus3 in His Position
        int DrawingAdjustment = 0;
        DrawingAdjustment = Tvarus3D;
        if (TvarusEM)
          DrawingAdjustment=DrawingAdjustment+4;
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), hevilSurfaceNum,
                  Vec4Si32(DrawingAdjustment*20, 0, DrawingAdjustment*20+20, 20));
      }
      if (currentLevel[xmas][ymas] == TVARUS4) {
        // activate Tvarus4
        tvarus4Active = true;
        // Set Tvarus4 Coordinates
        Tvarus4X = xmas;
        Tvarus4Y = ymas;
        // Paint Tvarus4 in His Position
        int DrawingAdjustment = 0;
        DrawingAdjustment = Tvarus4D;
        if (TvarusEM)
          DrawingAdjustment=DrawingAdjustment+4;
        BlitImage(Vec2Si32(36+xmas*16, 36+ymas*16), mevilSurfaceNum,
                  Vec4Si32(DrawingAdjustment*20, 0, DrawingAdjustment*20+20, 20));
      }
    }
  
  // CHECK ON LEVEL COMPLETENCE
  if (!FreeBoxes && !levelComplete && !secret)
    LevelComplete();
  return;
  
}

void OnPauseTimer() {
  // x-coord of tvarus
  static int screenCounter = 0;
  // does tvarus go right ?
  static bool goRight = true;
  // stage of animation of tvarus
  static int cader = 0;
  // offset from the begining of tvarus-image(green1);
  int update;
  // cloud1 x-coord
  static float cloud1x=float(1000);
  // cloud2 x-coord
  static float cloud2x=float(-300);
  // cloud3 x-coord
  static float cloud3x=float(300);
  // cloud1 y-coord
  static int cloud1y=Random32(0, 99);
  // cloud2 y-coord
  static int cloud2y=Random32(0, 99);
  // cloud3 y-coord
  static int cloud3y=Random32(0, 99);
  // Lightning rnd
  int lightning = Random32(0, 99);
  int lightCader = cader;
  if (cader > 2)
    lightCader = 2;
  // does cloud1 move right ?
  static bool cloud1Right = false;
  // does cloud2 move right ?
  static bool cloud2Right = true;
  // does cloud3 move right ?
  static bool cloud3Right = false;
  
  static int cloud1L;
  static int cloud1R;
  static int cloud1P;
  
  static int cloud2L;
  static int cloud2R;
  static int cloud2P;
  
  static int cloud3L;
  static int cloud3R;
  static int cloud3P;
  
  static int tvarusR;
  
  
  cader++;
  if (cader > 3)
    cader = 0;
  if (goRight) {
    screenCounter = screenCounter + 2;
    update = cader;
    if (screenCounter > 640)
      goRight = false;
  } else {
    screenCounter = screenCounter - 2;
    update = cader+4;
    if (screenCounter < 1)
      goRight = true;
  }
  tvarusR = update * 62 + 61;
  if (screenCounter > 576)
    tvarusR = 61 - (screenCounter - 576) + update * 62;
  
  
  if (cloud1Right) {
    cloud1x += float(0.5);
    if (cloud1x > 640) {
      cloud1Right = false;
      cloud1y = Random32(0, 99);
    }
  } else {
    cloud1x -= float(0.5);
    if (cloud1x < -300) {
      cloud1Right = true;
      cloud1y = Random32(0, 99);
    }
  }
  
  cloud1L = 0;
  cloud1R = 300;
  cloud1P = int(cloud1x);
  if (cloud1x > 340) {
    cloud1L = 0;
    cloud1R = int(300 - (cloud1x - 340));
    cloud1P = int(cloud1x);
  }
  if (cloud1x < 0) {
    cloud1L = int(-cloud1x);
    cloud1R = 300;
    cloud1P = 0;
  }
  
  if (cloud2Right) {
    cloud2x += float(0.5);
    if (cloud2x > 640) {
      cloud2Right = false;
      cloud2y = Random32(0, 99);
    }
  } else {
    cloud2x -= float(0.5);
    if (cloud2x < -300) {
      cloud2Right = true;
      cloud2y = Random32(0, 99);
    }
  }
  
  cloud2L = 0;
  cloud2R = 300;
  cloud2P = int(cloud2x);
  if (cloud2x > 340) {
    cloud2L = 0;
    cloud2R = int(300 - (cloud2x - 340));
    cloud2P = int(cloud2x);
  }
  if (cloud2x < 0) {
    cloud2L = int(-cloud2x);
    cloud2R = int(300);
    cloud2P = 0;
  }
  
  if (cloud3Right) {
    cloud3x += float(0.5);
    if (cloud3x > 640) {
      cloud3Right = false;
      cloud3y = Random32(0, 99);
    }
  } else {
    cloud3x -= float(0.5);
    if (cloud3x < -300) {
      cloud3Right = true;
      cloud3y = Random32(0, 99);
    }
  }
  
  cloud3L = 0;
  cloud3R = 300;
  cloud3P = int(cloud3x);
  if (cloud3x > 340) {
    cloud3L = 0;
    cloud3R = int(300 - (cloud3x - 340));
    cloud3P = int(cloud3x);
  }
  if (cloud3x < 0) {
    cloud3L = int(-cloud3x);
    cloud3R = 300;
    cloud3P = 0;
  }
  
  
  Clear();
  // Blit background
  BlitImage(Vec2Si32(0, 0), backgrndSurfaceNum, Vec4Si32(0, 0, 640, 480));
  // Paint Red Tvarus in His Position
  BlitImage(Vec2Si32(480, 290), red1SurfaceNum, Vec4Si32(cader*62, 0, cader*62+61, 56));
  // Paint Green Tvarus in His Position
  BlitImage(Vec2Si32(screenCounter, 400), green1SurfaceNum, Vec4Si32(update*62, 0, tvarusR, 42));
  // Paint Cloud1 in it's position
  BlitImage(Vec2Si32(cloud1P,cloud1y), cloud1SurfaceNum, Vec4Si32(cloud1L, 0, cloud1R, 100));
  // Paint Cloud2 in it's position
  BlitImage(Vec2Si32(cloud2P,cloud2y), cloud2SurfaceNum, Vec4Si32(cloud2L, 0, cloud2R, 100));
  // Paint Cloud3 in it's position
  BlitImage(Vec2Si32(cloud3P,cloud3y), cloud3SurfaceNum, Vec4Si32(cloud3L, 0, cloud3R, 100));
  // Paint light in it's position
  if (lightning < 1) {
    BlitImage(Vec2Si32(int(cloud3x+100),int(cloud3y+50)), lightSurfaceNum,
              Vec4Si32(lightCader*100, 0, lightCader*100+100, 100));
    SayThunder();
  }
  if (pauseOn) {
    // Blit "PAUSE"
    BlitImage(Vec2Si32(150, 158), pauseSurfaceNum, Vec4Si32(0, 0, 340, 64));
  }
  // Blit stone1
  BlitImage(Vec2Si32(0, 192), stone1SurfaceNum, Vec4Si32(0, 0, 127, 288));
  // Show the game screen.
  return;
}

void OnTimer() {
  if (!tvarusActive && !tvarus2Active && !tvarus3Active && !tvarus4Active)
    return;
  bool TEM = TvarusEM;
  if (TEM)
    TvarusEM = false;
  if (!TEM)
    TvarusEM = true;
  if (tvarusActive)
    TvarusMakesMove();
  if (tvarus2Active)
    Tvarus2MakesMove();
  if (tvarus4Active)
    Tvarus4MakesMove();
  if (tvarus3Active)
    Tvarus3MakesMove();
  return;
}

void TvarusMakesMove() {
  // Create LOCAL Variables
  int BackMoves[35][25];
  // -1 indicates unchecked
  // 0 indicates a man
  // -3 indicates tvarus
  // initialize BackMoves !!!
  for (int ymas = 0; ymas < 25; ymas++)
    for (int xmas = 0; xmas < 35; xmas++) {
      BackMoves[xmas][ymas] = -1;
      if ((currentLevel[xmas][ymas] == MAN)||
          (currentLevel[xmas][ymas] == CMAN)||
          (currentLevel[xmas][ymas] == HMAN))
        BackMoves[xmas][ymas] = 0;
      if (currentLevel[xmas][ymas] == TVARUS)
        BackMoves[xmas][ymas] = -3;
    }
  // now BackMoves is initialized !!!
  
  // Max number of moves
  // (not forever)
  int maxnum = 5;
  
  // Current move's number
  // now is tvarus'es number
  int currentnum = 0;
  
  // search for way !
  while (maxnum + 5 > currentnum) {
    for (int ymas = 0; ymas < 25; ymas++)
      for (int xmas = 0; xmas < 35; xmas++){
        if (BackMoves[xmas][ymas] == currentnum) {
          // this quad shold be checked !!!
          // let's look around:
          
          // YMT - Y move
          int YMT = 0;
          // XMT - X move
          int XMT = 0;
          
          // Check four directions
          for (XMT = -1; XMT < 2 ; XMT++)
            for (YMT = -1; YMT < 2; YMT++) {
              //check on possiblenes of move
              if (((XMT == 1 || XMT == -1) && (YMT == -1 || YMT == 1)) == false) {
                // it's possible to make move here, but
                // what is here ?
                
                // here is TVARUS
                if (currentLevel[xmas + XMT][ymas + YMT] == TVARUS) {
                  // tvarus gonna move to emptyness
                  if (currentLevel[xmas][ymas] == CLEAR) {
                    // move here
                    currentLevel[TvarusX][TvarusY] = CLEAR;
                    currentLevel[xmas][ymas] = TVARUS;
                    SetTvarusDirection(XMT, YMT);
                    PaintScreen();
                    return;
                  }
                  
                  // tvarus gonna move to a man!!!
                  if (currentLevel[xmas][ymas] == MAN) {
                    // move there !!!
                    currentLevel[TvarusX][TvarusY] = CLEAR;
                    currentLevel[xmas][ymas] = TVARUS;
                    SetTvarusDirection(XMT, YMT);
                    SayDeath();
                    PaintScreen();
                    //man is eaten now !!!
                    //
                    //
                    //
                    //you shold launch a function of death
                    ///
                    ///
                    ///
                    //and return
                    return;
                  }
                }
                // here is a CLEAR
                if (currentLevel[xmas + XMT][ymas + YMT] == CLEAR) {
                  // what is in BACKMOVES ??
                  if (BackMoves[xmas + XMT][ymas + YMT] == -1) {
                    BackMoves[xmas + XMT][ymas + YMT] = currentnum + 1;
                    if (maxnum < currentnum + 1)
                      maxnum = currentnum + 1;
                  }
                }
              }
            }
        }
      }
    currentnum++;
  }
}

void Tvarus2MakesMove() {
  // Create LOCAL Variables
  int BackMoves[35][25];
  // -1 indicates unchecked
  // 0 indicates a man
  // -3 indicates tvarus
  // initialize BackMoves !!!
  for (int ymas = 0; ymas < 25; ymas++)
    for (int xmas = 0; xmas < 35; xmas++) {
      BackMoves[xmas][ymas] = -1;
      if ((currentLevel[xmas][ymas] == MAN)||
          (currentLevel[xmas][ymas] == CMAN)||
          (currentLevel[xmas][ymas] == HMAN))
        BackMoves[xmas][ymas] = 0;
      if (currentLevel[xmas][ymas] == TVARUS2)
        BackMoves[xmas][ymas] = -3;
    }
  // now BackMoves is initialized !!!
  
  // Max number of moves
  // (not forever)
  int maxnum = 5;
  
  // Current move's number
  // now is tvarus'es number
  int currentnum = 0;
  
  // search for way !
  while (maxnum + 5 > currentnum) {
    for (int ymas = 0; ymas < 25; ymas++)
      for (int xmas = 0; xmas < 35; xmas++) {
        if (BackMoves[xmas][ymas] == currentnum) {
          // this quad shold be checked !!!
          // let's look around:
          
          // YMT - Y move
          int YMT = 0;
          // XMT - X move
          int XMT = 0;
          
          // Check four directions
          for (YMT = 1; YMT >- 2 ; YMT--)
            for (XMT = 1; XMT >- 2; XMT--) {
              //check on possiblenes of move
              if (((XMT == 1 || XMT == -1) && (YMT == -1 || YMT == 1)) == false) {
                // it's possible to make move here, but
                // what is here ?
                
                // here is TVARUS2
                if (currentLevel[xmas + XMT][ymas + YMT] == TVARUS2) {
                  // tvarus2 gonna move to emptyness
                  if (currentLevel[xmas][ymas] == CLEAR) {
                    // move here
                    currentLevel[Tvarus2X][Tvarus2Y] = CLEAR;
                    currentLevel[xmas][ymas] = TVARUS2;
                    SetTvarus2Direction(XMT, YMT);
                    PaintScreen();
                    return;
                  }
                  
                  // tvarus2 gonna move to a man!!!
                  if (currentLevel[xmas][ymas] == MAN) {
                    // move there !!!
                    currentLevel[Tvarus2X][Tvarus2Y] = CLEAR;
                    currentLevel[xmas][ymas] = TVARUS2;
                    SetTvarus2Direction(XMT, YMT);
                    SayDeath();
                    PaintScreen();
                    //man is eaten now !!!
                    //
                    //
                    //
                    //you shold launch a function of death
                    ///
                    ///
                    ///
                    //and return
                    return;
                  }
                }
                // here is a CLEAR
                if ((currentLevel[xmas + XMT][ymas + YMT] == CLEAR)||
                    (currentLevel[xmas + XMT][ymas + YMT] == TVARUS)) {
                  // what is in BACKMOVES ??
                  if (BackMoves[xmas + XMT][ymas + YMT] == -1) {
                    BackMoves[xmas + XMT][ymas + YMT] = currentnum + 1;
                    if (maxnum < currentnum + 1)
                      maxnum = currentnum + 1;
                  }
                }
              }
            }
        }
      }
    currentnum++;
  }
}

void Tvarus3MakesMove() {
  // Tvarus3 runs away from Tvarus4 and Man
  //							(BadGuys)
  // So he :
  //
  //		1. Starts cycle
  //			1.1 Finds distance1 from that point to himself
  //			1.2 Finds distance2 from that point to BadGuys
  //			1.3 Differ = distance1 - distance2
  //			1.4 MaxDiffer = Differ
  //			1.5 MaxDifferX = point's X
  //			1.6 MaxDifferY = point's Y
  //			end of cycle
  //
  //		2. Moves To (MaxDifferX, MaxDidfferY), or
  //			stays on his place, if MaxDiffer = 0.
  //
  
  // I-st step : he initiates all the variables.
  
  MaxDiffer = 0;
  MaxDifferX = 0;
  MaxDifferY = 0;
  int Differ = 0;
  int Distance2 = 0;
  Ui32 randNum = Random32(0, 4);
  bool mach = false;
  
  // II-nd step : he starts cycle, checking all the quads.
  
  
  // 1) Check right quad
  if ((currentLevel[Tvarus3X + 1][Tvarus3Y] == CLEAR)&&
     (turesMap[Tvarus3X + 1][Tvarus3Y] == CLEAR)) {
    Distance2 = FindDistance2(Tvarus3X + 1, Tvarus3Y);
    if (Distance2 > 0) {
      // Find Differ ....
      Differ = Distance2;
      if (Differ > MaxDiffer) {
        // Change optimal quad.
        MaxDiffer = Differ;
        MaxDifferX = Tvarus3X + 1;
        MaxDifferY = Tvarus3Y;
      }
    }
  }
  
  // 2) Check left quad
  if ((currentLevel[Tvarus3X - 1][Tvarus3Y] == CLEAR)&&
     (turesMap[Tvarus3X - 1][Tvarus3Y] == CLEAR)) {
    Distance2 = FindDistance2(Tvarus3X - 1, Tvarus3Y);
    if (Distance2 > 0){
      // Find Differ ....
      Differ = Distance2;
      if (Differ >= MaxDiffer) {
        // Change optimal quad.
        if ((MaxDiffer == 0)||(MaxDiffer < Differ)) {
          MaxDiffer = Differ;
          MaxDifferX = Tvarus3X-1;
          MaxDifferY = Tvarus3Y;
        }
        
        if (MaxDiffer == Differ) {
          if (randNum > 3) {
            mach = true;
            MaxDiffer = Differ;
            MaxDifferX = Tvarus3X - 1;
            MaxDifferY = Tvarus3Y;
          }
        }
      }
    }
  }
  
  // 3) Check upper quad
  if ((currentLevel[Tvarus3X][Tvarus3Y + 1] == CLEAR)&&
      (turesMap[Tvarus3X][Tvarus3Y + 1] == CLEAR)) {
    Distance2 = FindDistance2(Tvarus3X, Tvarus3Y + 1);
    if (Distance2 > 0){
      // Find Differ ....
      Differ = Distance2;
      if (Differ >= MaxDiffer) {
        // Change optimal quad.
        if (MaxDiffer == 0 || MaxDiffer < Differ) {
          MaxDiffer = Differ;
          MaxDifferX = Tvarus3X;
          MaxDifferY = Tvarus3Y+1;
        }
        
        if (MaxDiffer == Differ) {
          if (randNum > 2 && !mach) {
            mach = true;
            MaxDiffer = Differ;
            MaxDifferX = Tvarus3X;
            MaxDifferY = Tvarus3Y+1;
          }
        }
      }
    }
  }
  
  // 4) Check lower quad
  if ((currentLevel[Tvarus3X][Tvarus3Y - 1] == CLEAR)&&
      (turesMap[Tvarus3X][Tvarus3Y - 1] == CLEAR)) {
    Distance2 = FindDistance2(Tvarus3X, Tvarus3Y - 1);
    if (Distance2 > 0){
      // Find Differ ....
      Differ = Distance2;
      if (Differ >= MaxDiffer) {
        // Change optimal quad.
        
        if (MaxDiffer == 0 || MaxDiffer < Differ) {
          MaxDiffer = Differ;
          MaxDifferX = Tvarus3X;
          MaxDifferY = Tvarus3Y - 1;
        }
        
        if (MaxDiffer == Differ) {
          if (randNum > 1 && !mach) {
            MaxDiffer = Differ;
            MaxDifferX = Tvarus3X;
            MaxDifferY = Tvarus3Y-1;
          }
        }
      }
    }
  }
  
  if (MaxDiffer > 0) {
    // Make Move To quad(MaxDistanceX; MaxDistanceY)
    MakeMoveToMaxDist();
  }
  // End of function.
}

int FindDistance2( int checkingX,int checkingY) {
  // Create LOCAL Variables
  int BackMoves[35][25];
  // -1 indicates unchecked
  // 0 indicates current place ()
  // -3 indicates tvarus4||MAN
  // initialize BackMoves !!!
  for (int ymas = 0; ymas < 25; ymas++)
    for (int xmas = 0; xmas < 35; xmas++) {
      BackMoves[xmas][ymas] = -1;
      if (xmas == checkingX && ymas == checkingY)
        BackMoves[xmas][ymas] = 0;
      if (currentLevel[xmas][ymas] == TVARUS4 || currentLevel[xmas][ymas] == MAN)
        BackMoves[xmas][ymas] = -3;
    }
  // now BackMoves is initialized !!!
  
  // Max number of moves
  // (not forever)
  int maxnum = 2;
  
  // Current move's number
  // now is tvarus'es number
  int currentnum = 0;
  
  // search for way !
  while (maxnum + 1 > currentnum) {
    for (int ymas = 1; ymas < 24; ymas++)
      for (int xmas = 1; xmas < 34; xmas++){
        if (BackMoves[xmas][ymas] == currentnum) {
          // this quad shold be checked !!!
          // let's look around:
          
          // YMT - Y move
          int YMT = 0;
          // XMT - X move
          int XMT = 0;
          
          // Check four directions
          for (YMT = 1; YMT >- 2 ; YMT--)
            for (XMT = 1; XMT >- 2; XMT--) {
              //check on possiblenes of move
              if (((XMT == 1 || XMT == -1) && (YMT == -1 || YMT == 1)) == false) {
                // it's possible to make move here, but
                // what is here ?
                
                // here is TVARUS4||MAN
                if (currentLevel[xmas + XMT][ymas + YMT] == TVARUS4 ||
                    currentLevel[xmas + XMT][ymas + YMT] == MAN)
                  return currentnum;
                
                // here is a CLEAR
                if (currentLevel[xmas + XMT][ymas + YMT] == CLEAR ||
                    currentLevel[xmas + XMT][ymas + YMT] == TVARUS3) {
                  // what is in BACKMOVES ??
                  if (BackMoves[xmas + XMT][ymas + YMT] == -1) {
                    BackMoves[xmas + XMT][ymas + YMT] = currentnum + 1;
                    if (maxnum < currentnum + 1)
                      maxnum = currentnum + 1;
                  }
                }
              }
            }
        }
      }
    currentnum++;
  }
  
  // end of function
  return 0;
}

void MakeMoveToMaxDist() {
  // Create LOCAL Variables
  int BackMoves[35][25];
  // -1 indicates unchecked
  // 0 indicates MaxDiffer(...X,...Y)
  // -3 indicates tvarus3
  // initialize BackMoves !!!
  for (int ymas = 1; ymas < 24; ymas++)
    for (int xmas = 1; xmas < 34; xmas++) {
      BackMoves[xmas][ymas] = -1;
      if (xmas == MaxDifferX && ymas == MaxDifferY)
        BackMoves[xmas][ymas] = 0;
      if (currentLevel[xmas][ymas] == TVARUS3)
        BackMoves[xmas][ymas] = -3;
    }
  // now BackMoves is initialized !!!
  
  // Max number of moves
  // (not forever)
  int maxnum = 5;
  
  // Current move's number
  // now is tvarus'es number
  int currentnum = 0;
  
  // search for way !
  while (maxnum + 5 > currentnum) {
    for (int ymas = 0; ymas < 25; ymas++)
      for (int xmas = 0; xmas < 35; xmas++) {
        if (BackMoves[xmas][ymas] == currentnum) {
          // this quad shold be checked !!!
          // let's look around:
          
          // YMT - Y move
          int YMT = 0;
          // XMT - X move
          int XMT = 0;
          
          // Check four directions
          for (YMT = 1; YMT >- 2 ; YMT--)
            for (XMT = 1; XMT >- 2; XMT--) {
              //check on possiblenes of move
              if (((XMT == 1 || XMT == -1) && (YMT == -1 || YMT == 1)) == false) {
                // it's possible to make move here, but
                // what is here ?
                
                // here is TVARUS3
                if (currentLevel[xmas + XMT][ymas + YMT] == TVARUS3)
                  // tvarus3 gonna move to emptyness
                  if (currentLevel[xmas][ymas] == CLEAR) {
                    // move here
                    currentLevel[Tvarus3X][Tvarus3Y] = CLEAR;
                    currentLevel[xmas][ymas] = TVARUS3;
                    SetTvarus3Direction(XMT, YMT);
                    PaintScreen();
                    return;
                  }
                
                // here is a CLEAR
                if ((currentLevel[xmas + XMT][ymas + YMT] == CLEAR)||
                    (currentLevel[xmas + XMT][ymas + YMT] == TVARUS)) {
                  // what is in BACKMOVES ??
                  if (BackMoves[xmas + XMT][ymas + YMT] == -1) {
                    BackMoves[xmas + XMT][ymas + YMT] = currentnum + 1;
                    if (maxnum < currentnum + 1)
                      maxnum = currentnum + 1;
                  }
                }
              }
            }
        }
      }
    currentnum++;
  }
  
  // end of function
}

void Tvarus4MakesMove() {
  // Create LOCAL Variables
  int BackMoves[35][25];
  // -1 indicates unchecked
  // 0 indicates a man(tvarus3)
  // -3 indicates tvarus4
  // initialize BackMoves !!!
  for (int ymas = 0; ymas < 25; ymas++)
    for (int xmas = 0; xmas < 35; xmas++){
      BackMoves[xmas][ymas] = -1;
      if ((!tvarus3Active &&
           (currentLevel[xmas][ymas] == MAN ||
            currentLevel[xmas][ymas] == CMAN ||
            currentLevel[xmas][ymas] == HMAN))||
          (tvarus3Active && currentLevel[xmas][ymas] == TVARUS3))
        BackMoves[xmas][ymas] = 0;
      if (currentLevel[xmas][ymas] == TVARUS4)
        BackMoves[xmas][ymas] = -3;
    }
  // now BackMoves is initialized !!!
  
  // Max number of moves
  // (not forever)
  int maxnum = 5;
  
  // Current move's number
  // now is tvarus'es number
  int currentnum = 0;
  
  
  // search for way !
  while (maxnum + 5 > currentnum) {
    for (int ymas = 0; ymas < 25; ymas++)
      for (int xmas = 0; xmas < 35; xmas++) {
        if (BackMoves[xmas][ymas] == currentnum) {
          // this quad shold be checked !!!
          // let's look around:
          
          // YMT - Y move
          int YMT = 0;
          // XMT - X move
          int XMT = 0;
          
          // Check four directions
          for (YMT = 1; YMT >- 2 ; YMT--)
            for (XMT = 1; XMT >- 2; XMT--) {
              //check on possiblenes of move
              if (((XMT == 1 || XMT == -1) && (YMT == -1 || YMT == 1)) == false) {
                // it's possible to make move here, but
                // what is here ?
                
                // here is TVARUS4
                if (currentLevel[xmas + XMT][ymas + YMT] == TVARUS4) {
                  // tvarus4 gonna move to emptyness
                  if (currentLevel[xmas][ymas] == CLEAR) {
                    // move here
                    currentLevel[Tvarus4X][Tvarus4Y] = CLEAR;
                    // make HOLE ??
                    if (futuresMap[Tvarus4X][Tvarus4Y] == FUTURES)
                      currentLevel[Tvarus4X][Tvarus4Y] = CLOUD;
                    currentLevel[xmas][ymas] = TVARUS4;
                    SetTvarus4Direction(XMT, YMT);
                    PaintScreen();
                    return;
                  }
                  
                  // tvarus4 gonna move to a man(tvarus3)!!!
                  if ((currentLevel[xmas][ymas] == MAN && !tvarus3Active) ||
                      (tvarus3Active && currentLevel[xmas][ymas] == TVARUS3)) {
                    // move there !!!
                    if (currentLevel[xmas][ymas] == MAN)
                      death3BufferNum.Play();
                    currentLevel[Tvarus4X][Tvarus4Y] = CLEAR;
                    currentLevel[xmas][ymas] = TVARUS4;
                    tvarus3Active = false;
                    SetTvarus4Direction(XMT, YMT);
                    PaintScreen();
                    //man is eaten now !!!
                    //
                    //you shold launch a function of death
                    ///
                    //and return
                    return;
                  }
                }
                // here is a CLEAR
                if ((currentLevel[xmas + XMT][ymas + YMT] == CLEAR)||
                    (currentLevel[xmas + XMT][ymas + YMT] == TVARUS)) {
                  // what is in BACKMOVES ??
                  if (BackMoves[xmas + XMT][ymas + YMT] == -1) {
                    BackMoves[xmas + XMT][ymas + YMT] = currentnum + 1;
                    if (maxnum < currentnum + 1)
                      maxnum = currentnum + 1;
                  }
                }
              }
            }
        }
      }
    currentnum++;
  }
}

void SetTvarusDirection(int XMT, int YMT) {
  int hDir = -XMT;
  int vDir = YMT;
  TvarusD = LEFT;
  if (hDir == 1)
    TvarusD = RIGHT;
  if (vDir == 1)
    TvarusD = UP;
  if (vDir == -1)
    TvarusD = DOWN;
}

void SetTvarus2Direction(int XMT,int YMT) {
  int hDir = -XMT;
  int vDir = YMT;
  Tvarus2D = LEFT;
  if (hDir == 1)
    Tvarus2D = RIGHT;
  if (vDir == 1)
    Tvarus2D = UP;
  if (vDir == -1)
    Tvarus2D = DOWN;
}

void SetTvarus3Direction(int XMT,int YMT) {
  int hDir = -XMT;
  int vDir = YMT;
  Tvarus3D = LEFT;
  if (hDir == 1)
    Tvarus3D = RIGHT;
  if (vDir == 1)
    Tvarus3D = UP;
  if (vDir == -1)
    Tvarus3D = DOWN;
}

void SetTvarus4Direction(int XMT,int YMT) {
  int hDir = -XMT;
  int vDir = YMT;
  Tvarus4D = LEFT;
  if (hDir == 1)
    Tvarus4D = RIGHT;
  if (vDir == 1)
    Tvarus4D = UP;
  if (vDir == -1)
    Tvarus4D = DOWN;
}

void GameComplete() {
  LoadTableOfResults();
  ShowTableOfResults();
  InputTableOfResults();
  SaveTableOfResults();
  menuOnline = true;
  menuOff = true;
  StartANewGame();
}

void DisplayNumber(char* s, Ui32 x, Ui32 y) {
  Ui32 len = (Ui32)strlen(s);
  // start cycling
  for (Ui32 i = 0; i < len; ++i) {
    // make string into int
    char ch = s[i];
    Ui32 digit = atoi(&ch);
    // paint number
    BlitImage(Vec2Si32(x + i * 7, y), numbers1SurfaceNum, Vec4Si32(digit * 7, 0, digit * 7 + 7, 12));
  }
}

void DisplayAllNum() {
  char s[10];
  sprintf(s, "%d", stepsNum);
  DisplayNumber(s, 250, 13);
  
  sprintf(s, "%d", lifesNum);
  DisplayNumber(s, 585, 13);
  
  sprintf(s, "%d", levelNum);
  DisplayNumber(s, 92, 13);
}

void LoadTableOfResults() {
  ifstream fin("data/scores.dat");
  for (int zoyt = 0; zoyt < 15; zoyt++) {
    fin >> hiName1[zoyt];
  }
  int zoyt;
  for (zoyt = 0; zoyt < 15; zoyt++) {
    fin >> hiName2[zoyt];
  }
  for (zoyt = 0; zoyt < 15; zoyt++) {
    fin >> hiName3[zoyt];
  }
  fin >> hiSteps1;
  fin >> hiSteps2;
  
  fin >> hiSteps3;
  fin.close();
}

void ShowTableOfResults() {
  //Blit hiName1
  ShowName(hiName1, 60, 50);
  //Blit hiName2
  ShowName(hiName2, 60, 100);
  //Blit hiName3
  ShowName(hiName3, 60, 150);
  
  char s[10];
  //Blit hiSteps1
  sprintf(s, "%d", hiSteps1);
  DisplayNumbers2(s, 500, 50);
  //Blit hiSteps2
  sprintf(s, "%d", hiSteps2);
  DisplayNumbers2(s, 500, 100);
  //Blit hiSteps3
  sprintf(s, "%d", hiSteps3);
  DisplayNumbers2(s, 500, 150);
}

void InputTableOfResults() {
  if (stepsNum < hiSteps1) {
    hiSteps3 = hiSteps2;
    hiSteps2 = hiSteps1;
    hiSteps1 = stepsNum;
    for(int a = 0; a < 15; a++) {
      hiName3[a] = hiName2[a];
      hiName2[a] = hiName1[a];
    }
    InputHiSteps(1);
    ShowTableOfResults();
    return;
  }
  if (stepsNum < hiSteps2) {
    hiSteps3 = hiSteps2;
    hiSteps2 = stepsNum;
    for(int a = 0; a < 15; a++)
      hiName3[a] = hiName2[a];
    InputHiSteps(2);
    Clear();
    ShowTableOfResults();
    return;
  }
  if (stepsNum < hiSteps3) {
    hiSteps3 = stepsNum;
    InputHiSteps(3);
    Clear();
    ShowTableOfResults();
    return;
  }
}

void SaveTableOfResults() {
  ofstream fout("data/scores.dat");
  for (int zoyt = 0; zoyt < 15; zoyt++) {
    if (hiName1[zoyt] == ' ')
      fout << "-";
    else
      fout << hiName1[zoyt];
  }
  int zoyt;
  for (zoyt = 0; zoyt < 15; zoyt++) {
    if (hiName2[zoyt] == ' ')
      fout << "-";
    else
      fout << hiName2[zoyt];
  }
  
  for (zoyt=0; zoyt < 15; zoyt++) {
    if (hiName3[zoyt] == ' ')
      fout << "-";
    else
      fout << hiName3[zoyt];
  }
  fout << hiSteps1 << "\n";
  fout << hiSteps2 << "\n";
  fout << hiSteps3 << "\n";
  fout.close();
}

void ShowName(char NameToShow[], int X, int Y) {
  for (int qwerty=0; qwerty <15; qwerty++) {
    int adnum;
    switch (NameToShow[qwerty]) {
      case 'f':  //
        adnum = 0;
        break;
      case ',': //
        adnum = 20;
        break;
      case 'd':
        adnum = 40;
        break;
      case 'u'://
        adnum = 60;
        break;
      case 'l'://
        adnum = 80;
        break;
      case 't'://
        adnum = 100;
        break;
      case ';'://
        adnum =	120;
        break;
      case 'p'://
        adnum = 140;
        break;
      case 'b'://
        adnum = 160;
        break;
      case 'r'://
        adnum = 180;
        break;
      case 'k'://
        adnum = 200;
        break;
      case 'v'://
        adnum = 220;
        break;
      case 'y'://
        adnum = 240;
        break;
      case 'j'://
        adnum = 260;
        break;
      case 'g'://
        adnum = 280;
        break;
      case 'h'://
        adnum = 300;
        break;
      case 'c'://
        adnum = 320;
        break;
      case 'n'://
        adnum = 340;
        break;
      case 'e'://
        adnum = 360;
        break;
      case 'a':
        adnum = 380;
        break;
      case '['://
        adnum = 400;
        break;
      case 'w'://
        adnum = 420;
        break;
      case 'x'://
        adnum = 440;
        break;
      case 'i'://
        adnum = 460;
        break;
      case 'o'://
        adnum = 480;
        break;
      case '\''://
        adnum = 500;
        break;
      case '.'://
        adnum = 520;
        break;
      case 'z'://
        adnum = 540;
        break;
      case 's'://
        adnum = 560;
        break;
      case 'm'://
        adnum = 580;
        break;
      case 'q'://
        adnum = 600;
        break;
      case ' ':// space
        adnum = 620;
        break;
      default:
        adnum = 620;
    }
    BlitImage(Vec2Si32(X + qwerty * 19, Y), letters1SurfaceNum, Vec4Si32(adnum, 0, adnum + 19, 20));
  }
}

void DisplayNumbers2(char* s, Ui32 x, Ui32 y) {
  Ui32 len = (Ui32)strlen(s);
  
  // start cycling
  for (Ui32 i = 0; i < len; ++i) {
    // make string into int
    char ch = s[i];
    Ui32 digit = atoi(&ch);
    
    // paint number
    BlitImage(Vec2Si32(x + i * 19, y), numbers2SurfaceNum, Vec4Si32(digit * 20, 0, digit * 20 + 19, 20));
  }
}

void InputHiSteps(int stringNum) {
  switch (stringNum) {
    case 1:
      entName = hiName1;
      break;
    case 2:
      entName = hiName2;
      break;
    case 3:
      entName = hiName3;
      break;
  }
  for (int a = 14; a >- 1; a--) {
    entName[a] = '-';
    Clear();
    ShowTableOfResults();
  }
  nameBufferNum.Play();
  enteringResult = true;
  enteringLetter = 0;
}

void OnSymbolEnteredInTable(Ui32 wParam) {
  switch (wParam) {
    case kKeyBackspace:
      if (enteringLetter > 0) {
        entName[enteringLetter] = '-';
        enteringLetter--;
        entName[enteringLetter] = '_';
        Clear();
        ShowTableOfResults();
      }
      break;
    case kKeyEnter:
      SaveTableOfResults();
      enteringResult=false;
      break;
  }
}

void OnChar(Ui32 symbol) {
  if (enteringResult == false)
    return;
  if (enteringLetter < 15) {
    if (symbol == kKeyBackspace)
      return;
    if (symbol >= 'A' && symbol <= 'Z') {
      symbol = symbol + 'a' - 'A';
    }
    entName[enteringLetter] = symbol;
    enteringLetter++;
    Clear();
    ShowTableOfResults();
  }
}

bool InitSound() {
  // Load hello.
  helloBufferNum.Load("data/hello.wav");
  // Load goodbye.
  goodbyeBufferNum.Load("data/goodbye.wav");
  
  // Load steps
  steps1BufferNum.Load("data/step1.wav");
  steps2BufferNum.Load("data/step2.wav");
  steps3BufferNum.Load("data/step3.wav");
  
  // Load push
  push1BufferNum.Load("data/push1.wav");
  push2BufferNum.Load("data/push2.wav");
  push3BufferNum.Load("data/push3.wav");
  
  // Load ship
  ship1BufferNum.Load("data/ship1.wav");
  ship2BufferNum.Load("data/ship2.wav");
  
  // Load name
  nameBufferNum.Load("data/entname.wav");
  
  // Load death
  death1BufferNum.Load("data/death1.wav");
  death2BufferNum.Load("data/death2.wav");
  death3BufferNum.Load("data/death3.wav");
  
  // Load heal
  healBufferNum.Load("data/heal.wav");
  
  // Load thunder
  thunderBufferNum.Load("data/thunder.wav");
  thunder1BufferNum.Load("data/thunder1.wav");
  
  // Say hello.
  helloBufferNum.Play();
  return true;
}

void SayStep() {
  int SoundNum = Random32(0, 2);
  if (SoundNum < 1) {
    steps1BufferNum.Play();
    return;
  }
  if (SoundNum < 2) {
    steps2BufferNum.Play();
    return;
  }
  if (SoundNum < 3) {
    steps3BufferNum.Play();
    return;
  }
}

void SayPush() {
  int SoundNum = Random32(0, 2);
  if (SoundNum < 1) {
    push1BufferNum.Play();
    return;
  }
  if (SoundNum < 2) {
    push2BufferNum.Play();
    return;
  }
  if (SoundNum < 3) {
    push3BufferNum.Play();
    return;
  }
}

void SayShip() {
  int SoundNum = Random32(0, 1);
  if (SoundNum < 1) {
    ship1BufferNum.Play();
    return;
  }
  if (SoundNum < 2) {
    ship2BufferNum.Play();
    return;
  }
}

void SayDeath() {
  int SoundNum = Random32(0, 1);
  if (SoundNum < 1) {
    death1BufferNum.Play();
    return;
  }
  if (SoundNum < 2) {
    death2BufferNum.Play();
    return;
  }
}

void SayThunder() {
  int SoundNum = Random32(0, 1);
  if (SoundNum < 1) {
    thunderBufferNum.Play();
    return;
  }
  if (SoundNum < 2) {
    thunder1BufferNum.Play();
    return;
  }
}
